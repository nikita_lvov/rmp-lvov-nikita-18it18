package com.example.calculator;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText element1, element2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        element1 = findViewById(R.id.num1);
        element2 = findViewById(R.id.num2);
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClick(View view) {

        int num1 = Integer.parseInt(element1.getText().toString());
        int num2 = Integer.parseInt(element2.getText().toString());
        int resSum = num1 + num2;
        AlertDialog.Builder resultBuilder = new AlertDialog.Builder(MainActivity.this);
        resultBuilder.setMessage(Integer.toString(resSum)).setCancelable(true);
        AlertDialog resultAlert = resultBuilder.create();
        resultAlert.setTitle("Результат сложения");
        resultAlert.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("element1", element1.getText().toString());
        outState.putString("element2", element2.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        element1.setText(savedInstanceState.getString("element1"));
        element2.setText(savedInstanceState.getString("element2"));

        super.onRestoreInstanceState(savedInstanceState);
    }
}
