package com.example.musicshporgalka;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ContentActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        webView = findViewById(R.id.webView);

        int position = getIntent().getIntExtra("story_key", 0);

        if (position == 0) {
            webView.loadUrl("file:///android_asset/Hip_hop.html");
        }
        if (position == 1) {
            webView.loadUrl("file:///android_asset/Rock.html");
        }
        if (position == 2){
            webView.loadUrl("file:///android_asset/Pop.html");
        }
        if (position == 3){
            webView.loadUrl("file:///android_asset/edm.html");
        }
        if (position == 4){
            webView.loadUrl("file:///android_asset/RnB.html");
        }
        webView.setWebViewClient(new MyWebViewClient());
    }

    /**
     * Метод, чтобы использовать историю просмотра ссылок внутри WebView при нажатии кнопки назад
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    /**
     * Метод, чтобы просматривать ссылки внутри приложения
     */
    private class MyWebViewClient extends WebViewClient {
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }

        // Для старых устройств
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}