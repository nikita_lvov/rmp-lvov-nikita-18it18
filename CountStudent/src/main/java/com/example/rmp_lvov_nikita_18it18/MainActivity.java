package com.example.rmp_lvov_nikita_18it18;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int counter = 0;
    private TextView countStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countStudent = findViewById(R.id.countStudent);
    }

    public void onClickAddStudent(View view) {
        counter++;
        countStudent.setText(String.valueOf(counter));
    }

    public void onClickReset(View view) {
        counter = 0;
        countStudent.setText(String.valueOf(counter));
    }
}
